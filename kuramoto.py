import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def kuramoto(t, theta, omega, K):
    N = len(theta)
    dtheta_dt = np.zeros(N)
    for i in range(N):
        dtheta_dt[i] = omega[i] + (K / N) * np.sum(np.sin(theta - theta[i]))
    return dtheta_dt


def kop(theta):
    return np.abs(np.mean(np.exp(1j * theta), axis=0))


def make_animation(thetas, K, t, N, M, interval=100):
    X, Y = np.meshgrid(np.arange(0, N), np.arange(0, M))
    fig, (axq, axk) = plt.subplots(1, 2, figsize=(2 * 2*max(N, M), 2*max(N, M)))
    fig.suptitle(f'Kuramoto Oscillators N={N}, K={K:.2f}', fontsize=24)
    axq.set_xticks([])
    axq.set_yticks([])

    q = axq.quiver(X, Y, np.cos(thetas[:,0]), np.sin(thetas[:,0]),
                   scale=2, scale_units='inches')

    kops = kop(thetas)
    line, = axk.plot(t, kops, marker="o", markevery=[-1])
    axk.set_xlabel('t [s]')
    axk.set_ylabel('KOP')

    axq.margins(0.15, 0.15)

    def update(frame):
        angles = thetas[:,frame]
        q.set_UVC(np.cos(angles), np.sin(angles))
        line.set_data(t[:frame], kops[:frame])

    ani = animation.FuncAnimation(fig, update, thetas.shape[1],
                                  interval=interval, repeat=False)

    return ani

if __name__ == '__main__':
    np.random.seed(42)
    # Parameters
    N = 5**2  # Number of oscillators. Use a square number!
    K = 0.8  # Coupling strength
    omega = np.random.uniform(low=0.9, high=1.1, size=N)  # Natural frequencies

    # Initial conditions
    theta0 = np.random.uniform(low=0, high=2*np.pi, size=N)

    # Solve the Kuramoto model using numerical integration
    tf = 20
    t = np.linspace(0, tf, 500, endpoint=False)
    solution = solve_ivp(kuramoto, (0, tf), theta0, args=(omega, K),
                         t_eval=t)

    t = solution.t
    thetas = solution.y

    plt.close('all')

    Np = Mp = int(np.sqrt(N))
    anim = make_animation(thetas, K, t, Np, Mp, interval=50)
    writer = animation.FFMpegWriter(fps=30)
    anim.save('kuramoto.avi', writer=writer, dpi=100)
    plt.show()
